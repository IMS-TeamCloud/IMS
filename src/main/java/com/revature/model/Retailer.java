package com.revature.model;


import javax.persistence.*;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Entity
@Table(name = "IMS_RETAILER")
public class Retailer {

    public Long R_ID;
    public String R_NAME;
    private Address address;

    public Retailer() {
    }

    @Id
    @Column(name = "R_ID")
    public Long getR_ID() {
        return R_ID;
    }

    public void setR_ID(Long r_ID) {
        R_ID = r_ID;
    }

    @Column(name = "R_NAME")
    public String getR_NAME() {
        return R_NAME;
    }

    public void setR_NAME(String r_NAME) {
        R_NAME = r_NAME;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "R_ADDRESS")
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
