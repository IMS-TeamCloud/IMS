package com.revature.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revature.Utils.LocalDateTimeConverter;
import com.revature.Utils.LocalDateTimeConverter;
import oracle.sql.TIMESTAMP;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 * Updated by Tyler Deans 7/02/2017
 */
@Entity
@Table(name = "IMS_RETAILER_SALE")
public class RetailerSale {
    public Long RS_ID;

    public int RS_PRODUCT_QUNATITY;
    private Double rsCost;
    public LocalDateTime RS_TS;

    private Retailer retailer;
    private Product product;

    public RetailerSale() {
    }

    @Id
    @Column(name = "RS_ID")
    public Long getRS_ID() {
        return RS_ID;
    }

    public void setRS_ID(Long RS_ID) {
        this.RS_ID = RS_ID;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="RS_RETAILER")
    public Retailer getRetailer() {
        return retailer;
    }

    public void setRetailer(Retailer retailer) {
        this.retailer = retailer;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="RS_PRODUCT")
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Column(name = "RS_PRODUCT_QUNATITY")
    public int getRS_PRODUCT_QUNATITY() {
        return RS_PRODUCT_QUNATITY;
    }

    public void setRS_PRODUCT_QUNATITY(int RS_PRODUCT_QUNATITY) {
        this.RS_PRODUCT_QUNATITY = RS_PRODUCT_QUNATITY;
    }

    @Column(name = "RS_COST")
    public Double getRsCost() {
        return rsCost;
    }

    public void setRsCost(Double rsCost) {
        this.rsCost = rsCost;
    }

    @Column(name = "RS_TS")
    @JsonFormat(pattern = "yyyy-dd-MM HH:mm")
    @Convert(converter = LocalDateTimeConverter.class)
    public LocalDateTime getRS_TS() {
        return RS_TS;
    }

    public void setRS_TS(LocalDateTime RS_TS) {
        this.RS_TS = RS_TS;
    }
}
