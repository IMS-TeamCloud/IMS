package com.revature.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * Project 1 webApp
 * Created by Mitul on 6/27/2017.
 */
@Entity
@Table(name="IMS_ADDRESS")
public class Address {
    private Long iD;
    private String A_Street;
    private String A_City;
    private String A_State;
    private String A_ZIP;

    public Address() {
    }

    @Id
    @Column(name="A_ID")
    public Long getA_ID() {
        return iD;
    }

    public void setA_ID(Long a_ID) {
        iD = a_ID;
    }

    @Column(name="A_Street")
    public String getA_Street() {
        return A_Street;
    }

    public void setA_Street(String a_Street) {
        A_Street = a_Street;
    }

    @Column(name="A_City")
    public String getA_City() {
        return A_City;
    }

    public void setA_City(String a_City) {
        A_City = a_City;
    }

    @Column(name="A_State")
    public String getA_State() {
        return A_State;
    }

    public void setA_State(String a_State) {
        A_State = a_State;
    }

    @Column(name="A_ZIP")
    public String getA_ZIP() {
        return A_ZIP;
    }

    public void setA_ZIP(String a_ZIP) {
        A_ZIP = a_ZIP;
    }

    @Override
    public String toString() {
        return "Address{" +
                "A_ID=" + iD +
                ", A_Street='" + A_Street + '\'' +
                ", A_City='" + A_City + '\'' +
                ", A_State='" + A_State + '\'' +
                '}';
    }
}
