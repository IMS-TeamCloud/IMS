package com.revature.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Entity
@Table(name = "IMS_RETAILER_INVENTORY")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RetailerInventory {
    public Long RI_ID;
    //public int RI_RETAILER;
    //public int RI_PRODUCT;
    public int RI_PRODUCT_QUANTITY;
    public int RI_PRODUCT_THRESHOLD;
    private Retailer retailer;
    private Product product;

    public RetailerInventory() {
    }

    @Id
    @Column(name = "RI_ID")
    public Long getRI_ID() {
        return RI_ID;
    }

    public void setRI_ID(Long RI_ID) {
        this.RI_ID = RI_ID;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="RI_RETAILER")
    public Retailer getRetailer() {
        return retailer;
    }

    public void setRetailer(Retailer retailer) {
        this.retailer = retailer;
    }
    /*
    @Column(name = "RI_RETAILER")
    public int getRI_RETAILER() {
        return RI_RETAILER;
    }

    public void setRI_RETAILER(int RI_RETAILER) {
        this.RI_RETAILER = RI_RETAILER;
    }
    */
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "RI_PRODUCT")
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    // There is a spelling error in the database
    // That is why the column is RI_PRODUCT_QUNATITY
    @Column(name = "RI_PRODUCT_QUNATITY")
    public int getRI_PRODUCT_QUANTITY() {
        return RI_PRODUCT_QUANTITY;
    }

    public void setRI_PRODUCT_QUANTITY(int RI_PRODUCT_QUANTITY) {
        this.RI_PRODUCT_QUANTITY = RI_PRODUCT_QUANTITY;
    }

    @Column(name = "RI_PRODUCT_THRESHOLD")
    public int getRI_PRODUCT_THRESHOLD() {
        return RI_PRODUCT_THRESHOLD;
    }

    public void setRI_PRODUCT_THRESHOLD(int RI_PRODUCT_THRESHOLD) {
        this.RI_PRODUCT_THRESHOLD = RI_PRODUCT_THRESHOLD;
    }
}
