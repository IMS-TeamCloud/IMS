package com.revature.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Entity
@Table(name = "IMS_PO_LINE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class POLine {
    public Long POL_ID;
    //public int POL_PO;
    //public int POL_PRODUCT;
    public int POL_QUANTITY;
    public int POL_COST;

    private Product product;
    private PurchaseOrder purchaseOrder;

    public POLine() {
    }

    @Id
    @Column(name = "POL_ID")
    public Long getPOL_ID() {
        return POL_ID;
    }

    public void setPOL_ID(Long POL_ID) {
        this.POL_ID = POL_ID;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "POL_PRODUCT")
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "POL_PO")
    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }
    
    @Column(name = "POL_QUANTITY")
    public int getPOL_QUANTITY() {
        return POL_QUANTITY;
    }

    public void setPOL_QUANTITY(int POL_QUANTITY) {
        this.POL_QUANTITY = POL_QUANTITY;
    }

    @Column(name = "POL_COST")
    public int getPOL_COST() {
        return POL_COST;
    }

    public void setPOL_COST(int POL_COST) {
        this.POL_COST = POL_COST;
    }
}
