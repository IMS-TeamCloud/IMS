package com.revature.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Entity
@Table(name = "IMS_SUPPLIER")
public class Supplier {
    public Long S_ID;
    public String S_NAME;
    private Address address;

    public Supplier() {
    }

    @Id
    @Column(name = "S_ID")
    public Long getS_ID() {
        return S_ID;
    }

    public void setS_ID(Long s_ID) {
        S_ID = s_ID;
    }

    @Column(name = "S_NAME")
    public String getS_NAME() {
        return S_NAME;
    }

    public void setS_NAME(String s_NAME) {
        S_NAME = s_NAME;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "S_ADDRESS")
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
