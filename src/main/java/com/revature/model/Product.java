package com.revature.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by tyler on 6/29/2017.
 */


@Entity
@Table(name="IMS_PRODUCT")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product {


    private Long productId;
    private String name;
    private Double supplierPrice;
    private Double retailerPrice;
    private Set<Category> categories;

    public Product() {}

    @Id
    @Column(name="P_ID")
    public Long getId() {
        return productId;
    }

    public void setId(Long id) {
        this.productId = id;
    }

    @Column(name="P_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="P_SUPPLIER_PRICE")
    public Double getSupplierPrice() {
        return supplierPrice;
    }

    public void setSupplierPrice(Double supplierPrice) {
        this.supplierPrice = supplierPrice;
    }

    @Column(name="P_RETAILER_PRICE")
    public Double getRetailerPrice() {
        return retailerPrice;
    }

    public void setRetailerPrice(Double retailerPrice) {
        this.retailerPrice = retailerPrice;
    }

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "products")
    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }
}
