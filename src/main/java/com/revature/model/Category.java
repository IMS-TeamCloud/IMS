package com.revature.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Entity
@Table(name="IMS_CATEGORY")
public class Category {
    private Long categoryId;
    private String C_NAME;
    @JsonIgnore
    private Set<Product> products;

    public Category() {}

    @Id
    @Column(name = "C_ID")
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Column(name = "C_NAME")
    public String getC_NAME() {
        return C_NAME;
    }

    public void setC_NAME(String c_NAME) {
        C_NAME = c_NAME;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "IMS_PRODUCT_CATEGORY", joinColumns = @JoinColumn(name = "C_ID"), inverseJoinColumns = @JoinColumn(name="P_ID"))
    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
