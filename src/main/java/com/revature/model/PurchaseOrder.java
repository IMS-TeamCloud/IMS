package com.revature.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revature.Utils.LocalDateTimeConverter;
import oracle.sql.TIMESTAMP;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 * Updated by Tyler Deans on 7/04/2017
 */
@Entity
@Table(name = "IMS_PURCHASE_ORDER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PurchaseOrder {

    public Long PO_ID;
    public LocalDateTime PO_TS;
    private Supplier supplier;
    private Retailer retailer;
    public Double PO_COST;
    //private Set<POLine> poLines;

    public PurchaseOrder() {
    }

    @Id
    @Column(name = "PO_ID")
    public Long getPO_ID() {
        return PO_ID;
    }

    public void setPO_ID(Long PO_ID) {
        this.PO_ID = PO_ID;
    }

    @Column(name = "PO_TS")
    @Convert(converter = LocalDateTimeConverter.class)
    @JsonFormat(pattern = "yyyy-dd-MM HH:mm")
    public LocalDateTime getPO_TS() {
        return PO_TS;
    }

    public void setPO_TS(LocalDateTime PO_TS) {
        this.PO_TS = PO_TS;
    }


    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="PO_SUPPLIER")
    public Supplier getSupplier (){return supplier;}

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="PO_RETAILER")
    public Retailer getRetailer() {
        return retailer;
    }

    public void setRetailer(Retailer retailer) {
        this.retailer = retailer;
    }

    @Column(name = "PO_COST")
    public Double getPO_COST() {
        return PO_COST;
    }

    public void setPO_COST(Double PO_COST) {
        this.PO_COST = PO_COST;
    }
    /*
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="POL_PO")
    public Set<POLine> getPoLines() {
        return poLines;
    }

    public void setPoLines(Set<POLine> poLines) {
        this.poLines = poLines;
    }
    */
}
