package com.revature.model;


/**
 * Created by slh on 7/7/2017.
 */

public class IncomingOrder {

    private PurchaseOrder po;
    private POLine line;

    IncomingOrder(){}

    public PurchaseOrder getPo() {
        return po;
    }

    public void setPo(PurchaseOrder po) {
        this.po = po;
    }

    public POLine getLine() {
        return line;
    }

    public void setLine(POLine line) {
        this.line = line;
    }
}
