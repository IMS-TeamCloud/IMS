package com.revature.controller;

import com.revature.Services.RetailerInventoryService;
import com.revature.model.RetailerInventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by tyler on 7/4/2017.
 */
@RestController
@Transactional
public class RetailerInventoryController {
    @Autowired
    RetailerInventoryService service;

    @RequestMapping(value="/inventory/", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RetailerInventory>> getInventories() {
        List<RetailerInventory> list = service.getAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/inventory/{id}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetailerInventory> getInventory(@PathVariable Long id) {
        RetailerInventory inventory = service.getById(id);
        return new ResponseEntity<>(inventory, HttpStatus.OK);
    }
}
