package com.revature.controller;

import com.revature.Services.RetailerService;
import com.revature.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
/**
 * Created by tyler on 6/29/2017.
 */
@RestController
public class RetailerController {
    @Autowired
    RetailerService service;

    @RequestMapping(value="/retailer/", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Retailer>> getRetailers() {
        List<Retailer> list = service.findAll();
        return new ResponseEntity<List<Retailer>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/retailer/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Retailer> getRetailer(@PathVariable Long id) {
        Retailer retailer = service.getById(id);

        if(retailer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(retailer, HttpStatus.OK);
    }
}
