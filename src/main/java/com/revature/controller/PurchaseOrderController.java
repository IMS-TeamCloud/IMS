package com.revature.controller;

import com.fasterxml.jackson.databind.*;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import com.revature.Services.PurchaseOrderService;
import com.revature.model.IncomingOrder;
import com.revature.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.revature.Services.IncomingOrderService;

import java.util.List;

/**
 * Created by tyler on 6/29/2017.
 */
@RestController
public class PurchaseOrderController {
    @Autowired
    PurchaseOrderService service;
    IncomingOrderService incomming;

    public void saveOrder (PurchaseOrder po){
        service.save(po);
    }

    @RequestMapping(value="/order/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PurchaseOrder>> getOrders() {
        List<PurchaseOrder> list = service.findAll();
        return new ResponseEntity<List<PurchaseOrder>>(list, HttpStatus.OK);
    }

    // Implement a method to create a new order
    @RequestMapping(value="/order/create", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Void> createOrder(@RequestBody String purchaseOrder ) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            IncomingOrder in = mapper.readValue(purchaseOrder, IncomingOrder.class);
            incomming.ProcessOrder(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //service.save(purchaseOrder);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/order/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseOrder> getOrder(@PathVariable Long id) {
        PurchaseOrder order = service.getById(id);
        if (order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
