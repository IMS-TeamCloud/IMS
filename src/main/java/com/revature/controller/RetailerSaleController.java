package com.revature.controller;

import com.revature.Services.ProductService;
import com.revature.Services.RetailerSaleService;
import com.revature.model.RetailerSale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by tyler on 6/29/2017.
 */
@RestController
public class RetailerSaleController {
    @Autowired
    RetailerSaleService service;

    @RequestMapping(value="/sale/", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RetailerSale>> getSales() {
        List<RetailerSale> salesList = service.findAll();
        return new ResponseEntity<List<RetailerSale>>(salesList, HttpStatus.OK);
    }

    @RequestMapping(value="/sale/create", method = RequestMethod.POST)
    public ResponseEntity<Void> createSale(@RequestBody RetailerSale retailerSale) {
        service.save(retailerSale);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/sale/{id}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetailerSale> getSale(@PathVariable Long id) {
        RetailerSale sale = service.findById(id);
        if (sale == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(sale, HttpStatus.OK);
    }
}
