package com.revature.controller;

import com.revature.Services.AddressService;
import com.revature.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by tyler on 7/3/2017.
 */
@RestController
@Transactional
public class AddressController {
    @Autowired
    AddressService service;

    @RequestMapping(value="/addresses/", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Address>> getAddresses() {
        List<Address> addressList = service.getAll();
        return new ResponseEntity<>(addressList, HttpStatus.OK);
    }
}
