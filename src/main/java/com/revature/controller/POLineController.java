package com.revature.controller;

import com.revature.Services.POLineService;
import com.revature.model.POLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by tyler on 7/6/2017.
 */
@RestController
public class POLineController {
    @Autowired
    POLineService service;

    public  void savePO (POLine line){
        service.save(line);
    }
    @RequestMapping(value="/orderline/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<POLine>> getOrderLines() {
        List<POLine> list = service.getAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value="/orderline/create", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createOrderLine(@RequestBody POLine poLine) {
        service.save(poLine);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/orderline/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<POLine> getOrderLine(@PathVariable Long id) {
        POLine poLine = service.getById(id);
        return new ResponseEntity<>(poLine, HttpStatus.OK);
    }
}
