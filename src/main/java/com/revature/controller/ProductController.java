package com.revature.controller;

import com.revature.Services.ProductService;
import com.revature.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by tyler on 6/29/2017.
 */
@RestController
@Transactional
public class ProductController {
    @Autowired
    ProductService service;

    @RequestMapping(value="/product/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> getProduct(@PathVariable Long id) {
        Product p = service.getById(id);

        if (p == null){
            System.out.println("No product with that id");
            return new ResponseEntity<Product>((HttpStatus.NOT_FOUND));
        }
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    @RequestMapping(value="/product/", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> prodList = service.findAll();
        return new ResponseEntity<>(prodList, HttpStatus.OK);
    }
}
