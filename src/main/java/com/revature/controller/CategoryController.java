package com.revature.controller;

import com.revature.Services.CategoryService;
import com.revature.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by tyler on 7/3/2017.
 */
@RestController
public class CategoryController {
    @Autowired
    CategoryService service;

    @RequestMapping(value="/categories/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<List<Category>> getCategories() {
        List<Category> catList = service.findAll();
        return new ResponseEntity<> (catList, HttpStatus.OK);
    }

}
