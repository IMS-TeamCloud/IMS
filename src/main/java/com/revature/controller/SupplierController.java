package com.revature.controller;

import com.revature.Services.SupplierService;
import com.revature.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by tyler on 6/29/2017.
 */
@RestController
public class SupplierController {
    @Autowired
    SupplierService service;

    @RequestMapping(value="/supplier/", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Supplier>> getSuppliers() {
        List<Supplier> list = service.findAll();
        return new ResponseEntity<List<Supplier>>(list, HttpStatus.OK);
    }

    @RequestMapping(value="/supplier/{id}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Supplier> getSuppliers(@PathVariable Long id) {
        Supplier supplier = service.getById(id);
        if (supplier == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(supplier, HttpStatus.OK);
    }
}
