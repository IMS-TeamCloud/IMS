package com.revature.Services;

import com.revature.dao.PurchaseOrderDao;
import com.revature.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 * Updated by Tyler Deans on 6/29/2017
 */
@Service
@Transactional
public class PurchaseOrderService {

    @Autowired
    PurchaseOrderDao dao;

    public List<PurchaseOrder> findAll() { return dao.findAll(); }

    public PurchaseOrder getById(Long id) { return dao.getOne(id); }

    public void save(PurchaseOrder newOrder) { dao.save(newOrder); }
}
