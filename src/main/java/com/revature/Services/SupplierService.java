package com.revature.Services;

import com.revature.dao.SupplierDao;
import com.revature.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Service
@Transactional
public class SupplierService {
    @Autowired
    SupplierDao dao;

    public List<Supplier> findAll() {
        return dao.findAll();
    }

    public Supplier getById(Long id) {return dao.getOne(id); }
}
