package com.revature.Services;


import com.revature.dao.RetailerDao;
import com.revature.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Service
@Transactional
public class RetailerService {
    @Autowired
    RetailerDao dao;

    public List<Retailer> findAll() { return dao.findAll(); }

    public Retailer getById(Long id) { return dao.getOne(id); }
}
