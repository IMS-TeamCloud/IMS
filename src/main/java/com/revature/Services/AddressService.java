package com.revature.Services;

import com.revature.dao.AddressDao;
import com.revature.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/28/2017.
 */
@Service
@Transactional
public class AddressService{

    @Autowired
    AddressDao dao;

    public Address getById(Long A_Id){
        return dao.findOne(A_Id);
    }

    public List<Address> getAll(){
        return dao.findAll();
    }
}
