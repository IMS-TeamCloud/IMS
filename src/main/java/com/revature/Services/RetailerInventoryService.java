package com.revature.Services;

import com.revature.dao.RetailerInventoryDao;
import com.revature.model.RetailerInventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 * Updated by Tyler Deans 7/04/2017
 */
@Service
@Transactional
public class RetailerInventoryService {
    @Autowired
    RetailerInventoryDao dao;

    public List<RetailerInventory> getAll() { return dao.findAll(); }

    public RetailerInventory getById(Long id) { return dao.findOne(id); }
}
