package com.revature.Services;

import com.revature.dao.CategoryDao;
import com.revature.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
@Service
@Transactional
public class CategoryService {
    @Autowired
    CategoryDao dao;

    public List<Category> findAll() { return dao.findAll(); }
}
