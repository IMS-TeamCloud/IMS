package com.revature.Services;

import com.revature.dao.POLineDao;
import com.revature.model.POLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 * Updated by Tyler Deans
 */
@Service
@Transactional
public class POLineService {
    @Autowired
    POLineDao dao;
    public List<POLine> getAll() { return dao.findAll(); }

    public void save(POLine poLine) { dao.save(poLine); }

    public POLine getById(Long id) { return dao.getOne(id); }
}
