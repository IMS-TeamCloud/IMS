package com.revature.Services;
import com.revature.dao.ProductDao;
import com.revature.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by tyler on 6/29/2017.
 */
@Service
@Transactional
public class ProductService {
    @Autowired
    ProductDao dao;

    public List<Product> findAll() {
        return dao.findAll();
    }

    public Product getById(Long id) { return dao.getOne(id); }
}
