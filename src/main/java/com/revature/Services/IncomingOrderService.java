package com.revature.Services;

import com.revature.controller.POLineController;
import com.revature.controller.PurchaseOrderController;
import com.revature.model.IncomingOrder;
import com.revature.model.POLine;
import com.revature.model.Product;
import com.revature.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by slh on 7/7/2017.
 */
public class IncomingOrderService {

    PurchaseOrderController POC;
    POLineController POLC;

    public void ProcessOrder (IncomingOrder in){
        PurchaseOrder prod;
        POLine pline;

        prod = in.getPo();
        pline = in.getLine();

//        service1.save(prod);
//        service2.save(pline);
        POC.saveOrder(prod);
        POLC.savePO(pline);

    }
}
