package com.revature.Services;

import com.revature.dao.RetailerSaleDao;
import com.revature.model.RetailerSale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 * Updated by Tyler Deans on 6/29/2017
 */
@Service
@Transactional
public class RetailerSaleService {
    @Autowired
    RetailerSaleDao dao;

    public List<RetailerSale> findAll() { return dao.findAll(); }

    public RetailerSale findMaxSale() {return dao.findTopByOrderByRsCostAsc(); }

    public void save(RetailerSale newSale) { dao.save(newSale); }

    public RetailerSale findById(Long id) { return dao.findOne(id); }

}
