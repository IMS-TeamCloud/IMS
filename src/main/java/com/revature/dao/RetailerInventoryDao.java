package com.revature.dao;

import com.revature.model.RetailerInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/29/2017.
 */
public interface RetailerInventoryDao extends JpaRepository<RetailerInventory, Long> {}
