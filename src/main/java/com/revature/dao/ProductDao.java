package com.revature.dao;

import com.revature.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tyler on 6/29/2017.
 */
@Repository
public interface ProductDao extends JpaRepository<Product, Long> {}
