package com.revature.dao;

import com.revature.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Project 1 webApp
 * Created by Mitul on 6/27/2017.
 */
@Repository
public interface AddressDao extends JpaRepository<Address, Long> {}
