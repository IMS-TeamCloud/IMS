package com.revature;

import com.revature.Services.*;
import com.revature.controller.*;
import com.revature.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ImsApplicationTests {
	@Autowired
	private ProductController prodControl;
	@Autowired
	private PurchaseOrderController purchaseOrderControl;
	@Autowired
	private RetailerController retailControl;
	@Autowired
	private RetailerSaleController retailSaleControl;
	@Autowired
	private SupplierController supControl;
	@Autowired
    private AddressService addServ;
	@Autowired
    private ProductService prodServ;
	@Autowired
    private PurchaseOrderService purOrderServ;
	@Autowired
    private RetailerService retailServ;
	@Autowired
    private SupplierService supServ;

	@Autowired
    private  RetailerSaleService retailerSaleService;

	@Test
	public void testProductController() {
		assertEquals("class com.revature.controller.ProductController", this.prodControl.getClass().toString());
	}
    @Test
    public void testPurchaseOrderController() {
        assertEquals("class com.revature.controller.PurchaseOrderController", this.purchaseOrderControl.getClass().toString());
    }
    @Test
    public void testRetailerController() {
        assertEquals("class com.revature.controller.RetailerController",this.retailControl.getClass().toString());
    }
    @Test
    public void testRetailerSaleController() {
        assertEquals("class com.revature.controller.RetailerSaleController",this.retailSaleControl.getClass().toString());
    }
    @Test
    public void testSupplierController() {
        assertEquals("class com.revature.controller.SupplierController",this.supControl.getClass().toString());
    }

    @Test
    public void testAddressServiceGetAddress() {

        Address existingAdd = addServ.getById(1L);

        if(existingAdd != null) {
            assertNotNull("Street isn't null", existingAdd.getA_Street());
            assertNotNull("City isn't null", existingAdd.getA_City());
            assertNotNull("State isn't null", existingAdd.getA_State());
        }

        assertNotNull("Object is not null", existingAdd);
    }

    @Test
    public void testProductServiceGetProduct() {

        Product existingProd = prodServ.getById(1L);

        if(existingProd != null) {
            assertNotNull("name isn't null",existingProd.getName());
            assertNotNull("retailer price isn't null", existingProd.getRetailerPrice());
            assertNotNull("supplier price isn't null", existingProd.getSupplierPrice());
        }

        assertNotNull("Object is not null", existingProd);
    }

    @Test
    public void testPurchaseOrderServicegetOrder(){
	    PurchaseOrder existingOrder = purOrderServ.getById(1L);

	    if(existingOrder != null){
	        assertNotNull("cost is not null", existingOrder.getPO_COST());
	        assertNotNull("retailer is not null", existingOrder.getRetailer());
	        assertNotNull("supplier is not null", existingOrder.getSupplier());
	        assertNotNull("timestamp is not null", existingOrder.getPO_TS());
        }

        assertNotNull("Object is not null", existingOrder);
    }

    @Test
    public void testRetailerServicegetRetsiler(){
        Retailer existingRetail = retailServ.getById(1L);

        if(existingRetail != null){
            assertNotNull("name is not null", existingRetail.getR_NAME());
            assertNotNull("address is not null", existingRetail.getAddress());
        }
    }

    @Test
    public void testSupplierServicegetRetsiler(){
        Supplier existingSup = supServ.getById(1L);

        if(existingSup != null){
            assertNotNull("name is not null", existingSup.getS_NAME());
            assertNotNull("address is not null", existingSup.getAddress());
        }
    }

    @Test
    public void testProductServicegetAll() {
        List<Product> productList = prodServ.findAll();

        if (productList.size() > 0) {
            assertNotNull(productList.get(1));
        }
    }
}
