app.controller("NewOrderCtrl", ['$scope', '$http', 'retailerService', 'supplierService', 'productService', 'orderLineService', '$q', 'IDservice', function($scope, $http, retailerService, supplierService, productService, orderLineService, $q, IDservice) {
    $http.defaults.headers.post["Content-Type"] = "text/plain";
    $scope.today = new Date();

    var hours = $scope.today.getHours();
    var minutes = $scope.today.getMinutes();
    
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
   
    $scope.time = hours + ":" + minutes;


    var day = $scope.today.getDate();
    var month = $scope.today.getMonth();
    var year = $scope.today.getFullYear();
    
    $scope.currentDateTime = year + "-" + day + "-" + month + " " + $scope.time;

    retailerService.getRetailers().then(function(res) {
        $scope.retailers = res;
    }).catch(function (err) {
        console.log("An error occured");
    }).then(function(){
        if(IDservice.retailName){
        $scope.selectedRetailer = IDservice.retailName; 
    }
    });

    supplierService.getSuppliers().then(function(res) {
        $scope.suppliers = res;
    }).catch(function(err) {
        console.log("An error occured");
    });

    productService.getProducts().then(function(res) {
        $scope.products = res;
    }).catch(function(err) {
        console.log("An error occured");
    });

    // if(IDservice.retailName && IDservice.productName){
    //     $scope.selectedRetailer = IDservice.retailName;
    //     $scope.selectedProduct =  IDservice.productName;   
    // }



    $scope.addOrder = function() {
        var orderLineObj = {
            'POL_ID': $scope.orderLineId,
            'POL_QUANTITY': $scope.quantity,
            'POL_COST': $scope.selectedProduct.supplierPrice * $scope.quantity,
            'product': $scope.selectedProduct
        };

        var orderObj = {
            'PO_ID': $scope.orderId,
            'po_TS': $scope.currentDateTime,
            'supplier': $scope.selectedSupplier,
            'retailer': $scope.selectedRetailer,
            'PO_COST': $scope.total,
            'poLines': orderLineObj
        };

        var orderObjStr = angular.toJson(orderObj);
        
        $http({
            method: 'POST',
            url: 'http://localhost:8085/order/create',
            data: orderObjStr,
            headers: {'Content-Type': 'application/JSON'}
        })
        .success(function(){            
              console.log("data sucessfuly sent");     
        })
        .error(function(data, status, headers, config){
            console.log('unable to submit data');
        });
    };

}]);