app.service("productService", function($http) {
    var self = this;

    self.getProducts = function(res, err) {
        var promise = $http({
            url: "http://localhost:8085/product/",
            method: "GET"
        });

        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    } 
});