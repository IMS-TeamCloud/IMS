app.service("orderLineService", function($http) {
    var self = this;

    self.getOrderLines = function(res, err) {
        var promise = $http({
            method: "GET",
            url: "http://localhost:8085/orderline/"
        });

        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    }

    self.getOrderLine = function(lineId, res, err) {
        var promise = $http({
            method: "GET",
            url: "http://localhost:8085/orderline/{" + lineId + "}"
        });

        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    }
    
});