app.service("supplierService", function($http) {
    var self = this;

    self.getSuppliers = function(res, err) {
        var promise = $http({
            url: "http://localhost:8085/supplier/",
            method: "GET"
        });
        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    }

    self.getSupplier = function(suppId, res, err) {
        var promise = $http({
            url: "http://localhost:8085/retailer/{" + suppId + "}",
            method: "GET"
        });

        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    }
});