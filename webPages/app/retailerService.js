app.service("retailerService", function($http) {
    var self = this;

    self.getRetailers = function(res, err) {
        var promise = $http({
            method: "GET",
            url: "http://localhost:8085/retailer/"
            
        });

        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    }

    self.getRetailer = function(retId, res, err) {
        var promise = $http({
             method: "GET",
            url: "http://localhost:8085/retailer/{" + retId + "}"
           
        });

        promise = promise.then(function(res) {
            return res.data;
        });
        return promise;
    }
});