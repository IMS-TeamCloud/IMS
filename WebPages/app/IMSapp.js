var app = angular.module("IMS_app", ['ui.router', 'ng-fusioncharts']);


app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider.state('dashboard', {
        url: '/dashboard',
           views: {
               '': {templateUrl: '/pages/dashboard.html'},
               'BW@dashboard':{templateUrl: '/pages/BW.html'},
               'invAlerts@dashboard':{templateUrl:'/pages/alerts.html'},
               'cat@dashboard':{templateUrl: '/pages/catagories.html'}               
           }

    });
        $stateProvider.state('retailers', {
        url: '/retailers',
        templateUrl: '/pages/retailers.html'
    });

    $stateProvider.state('suppliers', {
        url: '/suppliers',
        templateUrl: '/pages/suppliers.html'
    });

    $stateProvider.state('purchases', {
        url: '/purchases',
        templateUrl: '/pages/purchases.html'
    });

    $stateProvider.state('products', {
        url: '/products',
        templateUrl: '/pages/products.html'
    });

    $stateProvider.state('sales', {
        url: '/sales',
        templateUrl: 'pages/sales.html'
    });
    $stateProvider.state('newOrder', {
       url: '/neworders',
       templateUrl: '/pages/newOrder.html',
       controller: 'NewOrderCtrl'
   })

    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/dashboard');
   
});

app.controller("index", function($scope, $state){
    $scope.Home= function(){
        $state.go('dashboard');
    }
    $scope.Suppliers = function(){
        $state.go('suppliers');
    }
    $scope.Retailers = function(){
        $state.go('retailers');
    }
    $scope.Products = function(){
        $state.go('products');
    }
    $scope.PuchaseOrders = function(){
        $state.go('purchases');
    }
    $scope.Sales = function(){
        $state.go('sales');
    }
    $scope.NewOrder = function(){
        $state.go('newOrder');
    }
})

app.controller('PieCharts', function($http, $filter, $q){
    var self = this;
    self.salesbystore = [];
    self.salesbycat = [];
    self.chartdata=[];
    var promise1 = $http({method: 'GET', url: "http://localhost:8085/sale/"});
    var promise2 = $http({method: 'GET', url: "http://localhost:8085/retailer/"}); 
    var promise3 = $http({method: 'GET', url: "http://localhost:8085/categories/"});
    self.fun = function(){
        $q.all([promise1, promise2, promise3]).then(function(data){ 
            self.salesFigures =data[0].data;
            self.retailer = data[1].data;
            self.categories = data[2].data;
            for (var i = 0; i < self.retailer.length; i++){
                var store;
                store= $filter("salesByStore")(self.retailer[i].R_NAME, self.salesFigures);
                self.salesbystore.push(store); 
            };
            for (var a = 0; a < self.salesbystore.length; a++){
                for(var b = 0; b < self.categories.length; b++){
                    var catsale;
                    var sum;
                    catsale = $filter("salesByCat")(self.salesbystore, self.categories[b].c_NAME);
                    sum = function(catsale, rsCost){
                        return catsale.reduce( function(a, b){
                        return a + b.rsCost;
                    }, 0)}(catsale);
                self.salesbycat.push({'store': self.salesbystore[a][0].retailer.R_NAME, 'label': self.categories[b].c_NAME ,'value': sum}); 
                }
            }
            
            for (var x = 0; x < self.retailer.length; x++){
                    var store;
                    self.chartdata = [];
                store= $filter("salesByStore2")(self.retailer[x].R_NAME, self.salesbycat);
                for (y=0; y<store.length; y++){
                    self.chartdata.push({"label": store[y].label, "value": store[y].value});
                }
                var chart = {
                    caption: store[0].store,
                    showlabels: "1",
                    showlegend: "0",
                    showpercentvalues: "1"
                };

                var pieChart;
                var pieName = 'pie'+ x
                 var pieChart = new FusionCharts.render({
                    width:"100%",
                    height:"190",
                    type:"pie2d",
                    renderAt: pieName,
                    dataFormat:"json",
                    dataSource:{
                    'chart': chart,
                    'data': self.chartdata
                }
                    
                }); 
                pieChart.render()
            };
        });

    }();
})

app.controller("bwData", function($http, $filter, $q){
    var self = this;
    self.salesFigures = [];
    self.retailer = [];
    self.sums=[];
    
    self.fun = function(){
        var promise1 = $http({method: 'GET', url: "http://localhost:8085/sale/"}); 
        var promise2 = $http({method: 'GET', url: "http://localhost:8085/retailer/"});
        $q.all([promise1, promise2]).then(function(data){ 
            self.salesFigures =data[0].data;
            self.retailer = data[1].data;
            for (var i = 0; i < self.retailer.length; i++){
                var store;
                var sum;
                store= $filter("salesByStore")(self.retailer[i].r_NAME, self.salesFigures);
                store=$filter("salesByMonth")(store);
                sum = function(store, rsCost){
                    return store.reduce( function(a, b){
                    return a + b.rsCost;
                }, 0)}(store);
                self.sums.push({'name': self.retailer[i].r_NAME, 'value': sum});     
            };
            FusionCharts.ready(function () {
           
            var Chart = new FusionCharts.render({
               id:"Sales",
                width:"100%",
                height:"190",
                type:"column2d",
                renderAt: 'chart-container',
                dataFormat:"json",
                dataSource:{
                    "chart": {
                        // caption cosmetics
                        "caption": "Sales by Store for the Month",
                        "captionFontSize": "12",
                        // more chart options & cosmetics
                    },
        
                    "data":[
                        {
                        "label": self.sums[0].name,
                        "value": self.sums[0].value,
                    }, 
                     {
                        "label": self.sums[1].name,
                        "value": self.sums[1].value,
                    }, 
                     {
                        "label": self.sums[2].name,
                        "value": self.sums[2].value,
                    }, 
                     {
                        "label": self.sums[3].name,
                        "value": self.sums[3].value,
                    },
                     {
                        "label": self.sums[4].name,
                        "value": self.sums[4].value,
                    },  ]  
                }
            });
            Chart.render();

        });
        });
    }();
});

app.controller("ProductCtrl", function ($scope, $http) {
    $scope.products = [];
    var fun = function(){
        $http({
            method: "GET",
            url: "http://localhost:8085/product/",

        }).success(function(response){
            $scope.products = response;
        }).error(function (response){
            console.log("error getting data");
        })           
    }(); 

});

app.controller("cat", ['$scope', '$http', 'cater', function ($scope, $http, cater) {
    $scope.categories = [];
    var fun = function(){
        $http({
            method: "GET",
            url: "http://localhost:8085/categories/",

        }).success(function(response){
            $scope.categories = response;

        }).error(function (response){
            console.log("error getting data");
        })           
    }();

        $scope.onChange = function(){
            cater.selectCategory=$scope.SelectedCats;  
        };
}]);

app.service("cater", function($http, $filter, $q){
    this.selectCategory = "All";
});

app.controller("PurchaseOrders", function ($scope, $http, $state) {
    $scope.POrders = [];
    var fun = function(){
        $http({
            method: "GET",
            url: "http://localhost:8085/order/",

        }).success(function(response){
            $scope.POrders = response;

        }).error(function (response){
            console.log("error getting data");
        })           
    }();   
});

app.controller("Retail", function ($scope, $http, $state) {
    $scope.retailers = [];
    var fun = function(){
        $http({
            method: "GET",
            url: "http://localhost:8085/retailer/",

        }).success(function(response){
            $scope.retailers = response;

        }).error(function (response){
            console.log("error getting data");
        })           
    }();   
});

app.controller("rs_sales", function ($scope, $http, $state) {
    $scope.sales = [];
    var fun = function(){
        $http({
            method: "GET",
            url: "http://localhost:8085/sale/",

        }).success(function(response){
            $scope.sales = response;

        }).error(function (response){
            console.log("error getting data");
        })           
    }();   
});

app.controller("Supplier", function ($scope, $http, $state) {
    $scope.suppliers = [];
    var fun = function(){
        $http({
            method: "GET",
            url: "http://localhost:8085/supplier/",

        }).success(function(response){
            $scope.suppliers = response;

        }).error(function (response){
            console.log("error getting data");
        })           
    }();   
});

app.controller('Alerts',['$scope', '$http', 'IDservice', '$state', function($scope, $http, IDservice, $state){
    $scope.inventory = [];
    var fun = function(){
        $http({
            method: 'GET',
            url: 'http://localhost:8085/inventory/',
        }).success(function(response){
            $scope.inventory= response;
        }).error(function(response){
            console.log('error getting data');
        })
    }();
    $scope.selectProduct= function(prodName, retName){
        if (!prodName || !retName){
            err();
        }else{
            IDservice.retailName = prodName;
            IDservice.productName = retName;
            $state.go('newOrder');
        }
    }
}]);

app.service("IDservice", function(){
    this.retailName;
    this.productName;
});

app.filter('Alert', function () {
  return function (items) {
    var filtered = [];
    for (var i = 0; i < items.length; i++) {
      var item = items[i];
      if (item.RI_PRODUCT_QUANTITY <= item.RI_PRODUCT_THRESHOLD) {
        filtered.push(item);
      }
    }
    return filtered;
  };
});

app.filter('filterCat', ['cater', function(cater){
    return function(items, selectCategory) {
        var filtered =[];
    if (cater.selectCategory == 'All'){
        return items;
    }else{
        for (var i = 0; i < items.length; i++) {
        var item = items[i];
        
            if(typeof(item.categories[0]) != "undefined"){
                if (item.categories[0].c_NAME == cater.selectCategory) {
                    filtered.push(item);
                } 
                 if (item.categories[1].c_NAME == cater.selectCategory) {
                    filtered.push(item);
                } 
            } 
        }
            return filtered;
        };    
    }   
  
}]);

app.filter('salesByCat', function(){
    return function(items, selectCategory) {
        var filtered =[];
    
        for (var i = 0; i < items.length; i++) {
        var items2 = items[i];
            for(var x =0; x< items2.length; x++){
                var item = items2[x];
                if(typeof(item.product.categories[0]) != "undefined"){
                    if (item.product.categories[0].c_NAME == selectCategory) {
                        filtered.push(item);
                    } 
                    if (item.product.categories[1].c_NAME == selectCategory) {
                        filtered.push(item);
                    } 
                } 
            }
            
        }
            return filtered;    
    }   
  
});

app.filter("salesByMonth", function(){
    return function(items){
        var filtered= [];
        d = new Date();
        // month = d.getMonth();
        for (var i = 0; i < items.length; i++){
            var item = items[i];
            var strDate = item.rs_TS.split("-");
            strDate2 = strDate[2].split(" ");
            if ((parseInt(strDate2[0]) == (parseInt(d.getMonth())-1)) && (parseInt(strDate[0]) == parseInt(d.getFullYear())) ){
                filtered.push(item);
            }
        }
        return filtered;
    };
});

app.filter("salesByStore", function(){
    
    return function(store, items){
        var filtered= [];  
        for (var i = 0; i < items.length; i++){
           
            var item = items[i];
            if (item.retailer.R_NAME == store){
                filtered.push(item);
            }
        }
        return filtered;
    };
})

app.filter("salesByStore2", function(){
    
    return function(store, items){
        var filtered= [];  
        for (var i = 0; i < items.length; i++){
            var item = items[i];
            if (item.store == store){
                filtered.push(item);
            }
        }
        return filtered;
    };
})

